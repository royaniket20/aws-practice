package com.aniket.aws.ec2.service.impl;

import AwsKmsKeyring_Compile.AwsKmsKeyring;
import com.aniket.aws.ec2.config.AwsKMSConfig;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * AWS KMS: KMS Operations and Encryption Context
 * AWS KMS allows you to specify an encryption context on kms:Encrypt. If you do so, you must provide the exact same encryption context on kms:Decrypt, or the operation will fail. (The match is case-sensitive, and key-value pairs are compared in an order independent way.)
 *
 * Behind the scenes, KMS is cryptographically binding the encryption context to the key material you are kms:Encrypt or kms:Decrypting as Additional Authenticated Data (AAD). In short, this is non-secret data that must be identical (not tampered-with or incomplete), or decryption fails.
 *
 * This feature defends against risks from ciphertexts being tampered with, modified, or replaced -- intentionally or unintentionally. It both defends against an attacker replacing one ciphertext with another as well as problems like operational events.
 *
 * For example, if a bad deployment swaps us-east-1.cfg with eu-central-1.cfg on your fleets, having { fleet: us-east-1 } asserted in us-east-1.cfg's encryption context will prevent it from accidentally being loaded by eu-central-1.
 */
public class KMSServiceImpl {
    private static final String KEY_ID = "b61fcb59-5874-409e-9d49-f8a7d8d9f9b9";
    private static final String KEY_ARN = "arn:aws:kms:us-east-1:427243927177:key/b61fcb59-5874-409e-9d49-f8a7d8d9f9b9";
    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
      String data = "I am a Secret Data";
        System.out.println(" Orginal Test = "+ data);
        Map<String, String> encryptionContext = new HashMap<>();
        encryptionContext.put("checker1","checker 1 value");
        encryptionContext.put("checker2","checker 2 value");

        EncryptRequest encryptRequest = EncryptRequest.builder().keyId(KEY_ARN).plaintext(SdkBytes.fromUtf8String(data)).encryptionContext(encryptionContext).build();
        AwsKMSConfig awsKMSConfig = new AwsKMSConfig();
        KmsClient kmsClient = awsKMSConfig.awskmsClient();
        EncryptResponse encryptResponse = kmsClient.encrypt(encryptRequest);
        //ciphertextBlob will have arn information - During Decrypt You need not to mention it again
        System.out.println(encryptResponse.responseMetadata().toString());
        SdkBytes encryptedData = encryptResponse.ciphertextBlob();
        System.out.println(" Encrypted Data less 4 KB = "+ encryptedData.asString(Charset.forName("ISO-8859-1")));
       //dECRYPTION OF tEXT
        //If we change context Slightly It will not work anymore
//        encryptionContext = new HashMap<>();
//        encryptionContext.put("checker1","checker 1 value");

        DecryptRequest decryptRequest = DecryptRequest.builder().ciphertextBlob(encryptedData).encryptionContext(encryptionContext).build();
        DecryptResponse decryptResponse = kmsClient.decrypt(decryptRequest);
        SdkBytes decryptedData = decryptResponse.plaintext();
        System.out.println(" Decrypted Data less 4 KB = "+ decryptedData.asUtf8String() );

        //Now use of Data Key
        data = "I am a LARGEEEE Secret Data";
        System.out.println(" Orginal Test = "+ data);
        GenerateDataKeyRequest generateDataKeyRequest = GenerateDataKeyRequest.builder().keyId(KEY_ARN).keySpec(DataKeySpec.AES_256).build();
        GenerateDataKeyResponse generateDataKeyResponse = kmsClient.generateDataKey(generateDataKeyRequest);
        SdkBytes plainDataKey = generateDataKeyResponse.plaintext();
        SdkBytes encryptedDataKey = generateDataKeyResponse.ciphertextBlob();
        System.out.println("Plan Data Key = "+plainDataKey.asString(Charset.forName("ISO-8859-1")));
        System.out.println("************************************");
        System.out.println("Encrypted Data Key  >>>>>>> \n"+ encryptedDataKey.asString(Charset.forName("ISO-8859-1")));

        //Do Vanila AES Encryption
        SecretKeySpec secretKeySpec = new SecretKeySpec(plainDataKey.asByteArray() , "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE,secretKeySpec);
        byte[] encryptedDataLarge = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        System.out.println(" Encrypted Large Data ------- ");
        System.out.println(new String(encryptedDataLarge,"ISO-8859-1"));

        //Now Decrypt Using encrypted Data Key on the Fly
         decryptRequest = DecryptRequest.builder().ciphertextBlob(encryptedDataKey).build();
         decryptResponse = kmsClient.decrypt(decryptRequest);
        plainDataKey = decryptResponse.plaintext();
        System.out.println("Plan Data Key Post Decryption = "+plainDataKey.asString(Charset.forName("ISO-8859-1")));

        //Do Vanila AES Decryption
         secretKeySpec = new SecretKeySpec(plainDataKey.asByteArray() , "AES");
         cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE,secretKeySpec);
        byte[] decryptedDataLarge = cipher.doFinal(encryptedDataLarge);
        System.out.println(" Decrypted Large Data ------- ");
        System.out.println(new String(decryptedDataLarge,StandardCharsets.UTF_8));

        System.out.println( "===CUSTOMER MASTER KEY  METADATA OF KMS KEY ========");

        DescribeKeyRequest describeKeyRequest = DescribeKeyRequest.builder().keyId(KEY_ARN).build();
        DescribeKeyResponse describeKeyResponse = kmsClient.describeKey(describeKeyRequest);
        System.out.println(describeKeyResponse.keyMetadata().arn());
        System.out.println(describeKeyResponse.keyMetadata().keyId());
        System.out.println(describeKeyResponse.keyMetadata().awsAccountId());
        System.out.println(describeKeyResponse.keyMetadata().cloudHsmClusterId()); //Wil only applicable for Customn Key Store
        System.out.println(describeKeyResponse.keyMetadata().creationDate());
        System.out.println(describeKeyResponse.keyMetadata().description());
        System.out.println(describeKeyResponse.keyMetadata().encryptionAlgorithmsAsStrings());


    }
}
