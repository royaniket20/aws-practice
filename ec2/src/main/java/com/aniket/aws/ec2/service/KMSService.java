package com.aniket.aws.ec2.service;

public interface KMSService {
    public  String encryptDataUsingMasterKey(String plainTextData);
    public  String decryptDataUsingMasterKey(String cypherTextData);
    public  String encryptDataUsingDataKey(String plainTextData);
    public  String decryptDataUsingDataKey(String cypherTextData);

}
