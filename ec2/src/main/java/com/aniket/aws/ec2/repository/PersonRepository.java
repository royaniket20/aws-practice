package com.aniket.aws.ec2.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.aniket.aws.ec2.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PersonRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public Person addPerson(Person person){
        dynamoDBMapper.save(person);
        return person;
    }

    public Person findPersonById(String personId){
        return dynamoDBMapper.load(Person.class,personId);
    }

    public Person removePerson(Person person){
        dynamoDBMapper.delete(person , dynamoDBDeleteExpression(person));
        return person;
    }
    private DynamoDBDeleteExpression dynamoDBDeleteExpression(Person person){
        //Condition to check if the Id exists or Not
        DynamoDBDeleteExpression dynamoDBDeleteExpression = new DynamoDBDeleteExpression();
        Map<String , ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
        expectedAttributeValueMap.put("personId" , new ExpectedAttributeValue(new AttributeValue().withS(person.getPersonId())));
        dynamoDBDeleteExpression.setExpected(expectedAttributeValueMap);
        return  dynamoDBDeleteExpression;
    }

    public  Person updatePerson(Person person){
     dynamoDBMapper.save(person,dynamoDBSaveExpression(person));
     return  findPersonById(person.getPersonId());
    }

    private DynamoDBSaveExpression dynamoDBSaveExpression(Person person){
        //Condition to check if the Id exists or Not
        DynamoDBSaveExpression dynamoDBSaveExpression = new DynamoDBSaveExpression();
        Map<String , ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
        expectedAttributeValueMap.put("personId" , new ExpectedAttributeValue(new AttributeValue().withS(person.getPersonId())));
        dynamoDBSaveExpression.setExpected(expectedAttributeValueMap);
        return  dynamoDBSaveExpression;
    }

    public List<Person> getAllPersons() {
        List<Person> personList = new ArrayList<>();
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
        //This is lazy loaded - you need to manually iterate
        PaginatedScanList<Person> peoples = dynamoDBMapper.scan(Person.class, scanExpression);
        for (Person person : peoples) {
            personList.add(person);
        }
        return personList;
    }
}
