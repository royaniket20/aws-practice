package com.aniket.aws.ec2.service.impl;

import com.aniket.aws.ec2.dto.StudentDto;
import com.aniket.aws.ec2.entity.Student;
import com.aniket.aws.ec2.repository.StudentRepository;
import com.aniket.aws.ec2.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;
    @Override
    public List<StudentDto> getAllStudents() {
        Iterable<Student> studentsList = studentRepository.findAll();
        List<StudentDto> studentDtos = new ArrayList<>();
        studentsList.forEach(item-> {
            StudentDto studentDto = new StudentDto();
            BeanUtils.copyProperties(item,studentDto);
            studentDtos.add(studentDto);
        });
        return studentDtos;
    }
}
