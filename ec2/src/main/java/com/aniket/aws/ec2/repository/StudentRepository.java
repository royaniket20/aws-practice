package com.aniket.aws.ec2.repository;

import com.aniket.aws.ec2.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student , String> {

}
