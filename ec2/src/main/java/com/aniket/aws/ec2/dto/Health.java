package com.aniket.aws.ec2.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Health {

    String name;
    Date  lastChecked;

}
