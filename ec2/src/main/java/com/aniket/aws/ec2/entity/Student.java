package com.aniket.aws.ec2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table
public class Student {

    @Id
    String id;

    @Column
    String name;

    @Column
    String address;


}
