package com.aniket.aws.ec2.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.aniket.aws.ec2.model.AWSSecret;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
@Slf4j
public class SecretConfig {
    public static final String ACCESS_KEY = "AKIAWG6NY62E5ILBPZMO";
    public static final String SECRET_KEY = "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";

    @Bean
    public DataSource dataSource(){
        AWSSecret awsSecret = getSecret();
        return DataSourceBuilder.create()
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .url("jdbc:"+awsSecret.getEngine()+"://"+awsSecret.getHost()+":"+awsSecret.getPort()+"/db")
                .username(awsSecret.getUsername())
                .password(awsSecret.getPassword())
                .build();
    }


    private AWSSecret getSecret() {
        AWSSecret awsSecret = new AWSSecret();
        String secretName = "my-db-credentials";
        String customSecretName= "my-custom-secret";

        // Create a Secrets Manager client
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY)))
                .build();
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
        try {
          GetSecretValueResult getSecretValueResponse = client.getSecretValue(getSecretValueRequest);
           if(getSecretValueResponse.getSecretString()!=null){
               String secret = getSecretValueResponse.getSecretString();
               System.out.println(" Given Data -----> "+secret);
               awsSecret = new Gson().fromJson(secret , AWSSecret.class);
           }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getSecretValueRequest = new GetSecretValueRequest().withSecretId(customSecretName);
        try {
            GetSecretValueResult getSecretValueResponse = client.getSecretValue(getSecretValueRequest);
            if(getSecretValueResponse.getSecretString()!=null){
                String secret = getSecretValueResponse.getSecretString();
                System.out.println("Given Data Custom -----> "+secret);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Your code goes here.
        return awsSecret;
    }

}
