package com.aniket.aws.ec2.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class DynamoDbConfig {

    public static final String SERVICE_ENDPOINT = "dynamodb.us-east-1.amazonaws.com";
    public static final String REGION = "us-east-1";
    public static final String ACCESS_KEY = "AKIAWG6NY62E5ILBPZMO";
    public static final String SECRET_KEY = "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";

    @Bean
    public DynamoDBMapper dynamoDBMapper(){

        DynamoDBMapper dynamoDBMapper = new DynamoDBMapper((amazonDynamoDBConfig()));
        log.info("******************************** {}",dynamoDBMapper);
        return dynamoDBMapper;
    }

    private AmazonDynamoDB amazonDynamoDBConfig() {

        return AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(SERVICE_ENDPOINT, REGION)).withCredentials(new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY))).build();
    }
}
