package com.aniket.aws.ec2.controller;


import com.aniket.aws.ec2.dto.StudentDto;
import com.aniket.aws.ec2.entity.Person;
import com.aniket.aws.ec2.repository.PersonRepository;
import com.aniket.aws.ec2.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class HomeController {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StudentService studentService;

    @Autowired
    private PersonRepository personRepository;

    @GetMapping
    public Health checkHealth(){
        try {
            // do some test to determine state of component
            return  Health.up().withDetail("Name" , applicationContext.getId()).withDetail("version", "1.0").build();
        }
        catch (Exception ex) {
            return Health.down(ex).build();
        }
    }

    @GetMapping("/students")
    public List<StudentDto> getAllStudents(){
        return  studentService.getAllStudents();
    }

    @GetMapping("/persons")
    public List<Person> getAllPersons(){
        return  personRepository.getAllPersons();
    }

    @PostMapping("/person")
    public Person addPerson(@RequestBody  Person person){
       return  personRepository.addPerson(person);
    }

    @GetMapping("/person/{personId}")
    public Person getPerson(@PathVariable String personId){
        return personRepository.findPersonById(personId);
    }

    @DeleteMapping("/person")
    public Person removePerson(@RequestBody  Person person){
        return personRepository.removePerson(person);
    }

    @PutMapping("/person")
    public Person updatePerson(@RequestBody  Person person){
        return personRepository.updatePerson(person);
    }




}
