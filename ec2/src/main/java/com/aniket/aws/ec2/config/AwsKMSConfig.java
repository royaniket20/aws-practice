package com.aniket.aws.ec2.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;

@Configuration
@Slf4j
public class AwsKMSConfig {


    public static final String ACCESS_KEY = "AKIAWG6NY62E5ILBPZMO";
    public static final String SECRET_KEY = "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";
    @Bean
    public KmsClient awskmsClient(){
        KmsClient kmsClient = KmsClient.builder().credentialsProvider(StaticCredentialsProvider.create(
                AwsBasicCredentials.create(ACCESS_KEY, SECRET_KEY))).region(Region.US_EAST_1).build();
        log.info(" Created KMS Client ------ {}", kmsClient);
        return kmsClient;
    }
}
