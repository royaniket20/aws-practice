package com.aniket.aws.ec2.model;

import lombok.Data;

@Data
public class AWSSecret {
    private String username;
    private String password;

    private String host;
    private String engine;

    private String port;

    private String dbInstanceIdentifier;

}


//{"username":"root","password":"password","engine":"mysql","host":"database-dev.cphoa36lr9jz.us-east-1.rds.amazonaws.com","port":3306,"dbInstanceIdentifier":"database-dev"}