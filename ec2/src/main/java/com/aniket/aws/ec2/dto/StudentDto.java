package com.aniket.aws.ec2.dto;
import lombok.Data;
@Data
public class StudentDto {
    String id;
    String name;

    String address;
}
