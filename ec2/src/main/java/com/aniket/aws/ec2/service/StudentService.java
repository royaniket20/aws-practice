package com.aniket.aws.ec2.service;

import com.aniket.aws.ec2.dto.StudentDto;

import java.util.List;

public interface StudentService {
    public List<StudentDto> getAllStudents();
}
