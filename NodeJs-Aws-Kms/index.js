const express = require("express");
const crypto = require("crypto");
const AWS = require("aws-sdk");
AWS.config.loadFromPath("./config.json");
const kmsInstance = new AWS.KMS();
const KmsParams = {
  KeyId:
    "arn:aws:kms:us-east-1:427243927177:key/b61fcb59-5874-409e-9d49-f8a7d8d9f9b9", // The identifier of the CMK to use to encrypt the data key. You can use the key ID or Amazon Resource Name (ARN) of the CMK, or the name or ARN of an alias that refers to the CMK.
  KeySpec: "AES_256", // Specifies the type of data key to return.
};
const {
  KmsKeyringNode,
  buildClient,
  CommitmentPolicy,
} = require("@aws-crypto/client-node");
const { encrypt, decrypt } = buildClient(
  CommitmentPolicy.REQUIRE_ENCRYPT_REQUIRE_DECRYPT
);
const app = express();
const port = 3000;
const generatorKeyId =
  "arn:aws:kms:us-east-1:427243927177:key/b61fcb59-5874-409e-9d49-f8a7d8d9f9b9";
//This is for double Wrapping of keys
const keyIds = [
  "arn:aws:kms:us-east-1:427243927177:key/ea67e0d9-d227-489f-8af9-3b3485c9d2bc",
  "arn:aws:kms:us-east-1:427243927177:key/487a42c8-69eb-48c8-bce3-4c2aa6e02997",
];
const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
const context = {
  accountId: "100",
  purpose: "youtube demo",
  country: "Sri Lanka",
};
const myText = "My passwords for senstive data";

app.get("/datakey", async (req, resp) => {
  const obj = {
    dataKey: "App is Up and Running",
    encryptedDataKey: new Date(),
  };

  let result = await generateDataKey();
  console.log(result);
  let initialData = Buffer.from("This is a very secret Data", "utf-8");
  console.log(`Inital Data : ${initialData}`);
  let encryptedData = encryptAES(result.Plaintext, initialData);
  console.log(`Encrypted  Data : ${encryptedData}`);
  console.log(`Decrypting the Data Key -------`);
  const CiphertextBlob = result.CiphertextBlob;
  const KeyId = KmsParams.KeyId;
  const decryptedDataKey = await decryptDataKey(CiphertextBlob, KeyId);
  console.log(decryptedDataKey);
  console.log("-------------------------------");
  const decryptedData = decryptAES(decryptedDataKey.Plaintext,encryptedData)
  console.log(` Decrypted Data -- Back to Original -----------`);
  console.log(decryptedData);
  console.log(`---------------------------------------------`);
  const plaintextFinalData = decryptedData.toString("ascii");
  console.log(plaintextFinalData);
  console.log(`---------------------------------------------`);

  return resp.json(obj);
});

function generateDataKey() {
  return new Promise((resolve, reject) => {
    kmsInstance.generateDataKey(KmsParams, (err, data) => {
      if (err) {
        console.log(`Some error has happend ------`);
        reject(err);
      } else {
        console.log(`Data key has been created  ------`);
        resolve(data);
      }
    });
  });
}

function decryptDataKey(CiphertextBlob, KeyId) {
  return new Promise((resolve, reject) => {
    kmsInstance.decrypt({ CiphertextBlob, KeyId }, (err, data) => {
      if (err) {
        console.log(`Some error has happend ------`);
        reject(err);
      } else {
        console.log(`Data key has been Decrypted  ------`);
        resolve(data);
      }
    });
  });
}

function encryptAES(key, strBuffer) {
  const algorithm = "AES-256-CBC";

  const iv = Buffer.from("00000000000000000000000000000000", "hex");

  const encryptor = crypto.createCipheriv(algorithm, key, iv);
  encryptor.write(strBuffer);
  encryptor.end();

  return encryptor.read();
}


function decryptAES(key, buffer) {
  const algorithm = 'AES-256-CBC';

  const iv = Buffer.from('00000000000000000000000000000000', 'hex');

  const decryptor = crypto.createDecipheriv(algorithm, key, iv);
  decryptor.write(buffer);
  decryptor.end();

  return decryptor.read();
}

app.get("/", async (req, res) => {
  const obj = {
    health: "App is Up and Running",
    lastChecked: new Date(),
  };
  console.log(`Original Data - ${myText}`);
  const { result } = await encrypt(keyring, myText, {
    encryptionContext: context,
  });
  console.log(`The Encrypted Data ====================`);
  console.log(result);
  console.log("-------------------------------------");
  /* Decrypt the data. */
  const { plaintext, messageHeader } = await decrypt(keyring, result);
  console.log(`The Decrypted Data ====================`);
  console.log(plaintext);
  console.log("-------------------------------------");
  const plaintextFinalData = plaintext.toString("ascii");
  console.log(plaintextFinalData);
  console.log("-------------------------------------");
  /* Grab the encryption context so you can verify it. */
  const { encryptionContext } = messageHeader;
  console.log(
    `Final Received Contex ---- ${JSON.stringify(encryptionContext)}`
  );
  return res.json(obj);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
