package com.aniket.sqsDemo.controller;

import com.aniket.sqsDemo.model.Health;
import io.awspring.cloud.sns.core.SnsTemplate;
import io.awspring.cloud.sqs.operations.SendResult;
import io.awspring.cloud.sqs.operations.SqsTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@Slf4j
public class HomeController {

    @Value("${spring.cloud.aws.sqs.endpoint}")
    private String queueEndpoint;

    @Value("${spring.cloud.aws.sns.endpoint}")
    private String topicEndpoint;

    @Autowired
    private SqsTemplate sqsTemplate;

    @Autowired
    private SnsTemplate snsTemplate;

    @GetMapping("/{message}")
    public Health helloData(@PathVariable String message)
    {
        sendNotification();
        SendResult<String> send = sqsTemplate.send(queueEndpoint, MessageBuilder.withPayload(message).build());
        log.info(" Message Sent with UUID - {}" , send.messageId());
        return Health.builder()
                .name(" LAL LAL LALA")
                .lastChecked(new Date())
                .build();
    }

    void sendNotification() {
        // sends String payload
        snsTemplate.sendNotification(topicEndpoint, "payload", "subject");
        // sends object serialized to JSON
        snsTemplate.sendNotification(topicEndpoint, new Health("John", new Date()), "subject person");
        // sends a Spring Messaging Message
        Message<String> message = MessageBuilder.withPayload("payload")
                .setHeader("header-name", "header-value")
                .build();
        snsTemplate.send(topicEndpoint, message);
    }
}
