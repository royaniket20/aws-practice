package com.aniket.sqsDemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class Health {

    String name;
    Date  lastChecked;

}
