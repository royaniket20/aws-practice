package com.aniket.sqsDemo.config;

import io.awspring.cloud.sns.core.SnsTemplate;
import io.awspring.cloud.sns.sms.SnsSmsTemplate;
import io.awspring.cloud.sqs.config.SqsMessageListenerContainerFactory;
import io.awspring.cloud.sqs.listener.acknowledgement.handler.AcknowledgementMode;
import io.awspring.cloud.sqs.operations.SqsTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sqs.SqsAsyncClient;

@Configuration
@Slf4j
public class AWSSNSConfig {

    @Value("${spring.cloud.aws.credentials.access-key}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secret-key}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Bean
    public SnsClient amazonSNS(){
        return SnsClient.builder()
                .region(Region.of(region))
                .credentialsProvider(StaticCredentialsProvider
                        .create(AwsBasicCredentials.create(accessKey, secretKey)))
                .build();
        // add more Options
    }

    @Bean
    public SnsTemplate snsTemplate(SnsClient snsClient){
        return new SnsTemplate(snsClient);
    }


    /**
     * There are three acknowledgment modes available:
     *
     * ON_SUCCESS – Acknowledges a message after successful processing.
     * ALWAYS – Acknowledges a message after processing, regardless of success or error.
     * MANUAL – The framework does not acknowledge messages automatically, and Acknowledgement objects can be received in the listener method. In this mode, you must send acknowledgments manually.
     * @param sqsAsyncClient
     * @return
     */
    @Bean
    SqsMessageListenerContainerFactory<Object> defaultSqsListenerContainerFactory(SqsAsyncClient sqsAsyncClient) {

        return SqsMessageListenerContainerFactory.builder()
                .configure(options -> options.acknowledgementMode(AcknowledgementMode.MANUAL))
                .acknowledgementResultCallback(new AckResultCallback())
                .sqsAsyncClient(sqsAsyncClient)
                .build();
    }

}
