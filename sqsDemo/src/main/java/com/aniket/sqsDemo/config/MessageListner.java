package com.aniket.sqsDemo.config;

import io.awspring.cloud.sqs.annotation.SqsListener;
import io.awspring.cloud.sqs.listener.acknowledgement.Acknowledgement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
@Slf4j
public class MessageListner {

    @SqsListener("test-queue" )
    public void listen(Message<?> message) {
        String payload = message.getPayload().toString();
        log.info("Message received on listen method at {} ===> {}", OffsetDateTime.now() , payload);
        if(payload.length()>10){
          log.error(" Message is Too Long");
            throw new RuntimeException(" I am not going to listen to It ");
        }else{
            Acknowledgement.acknowledge(message);
        }
    }

}
